var app = new Vue({
    el: '#app',
    data: {
        windowWidth: 0,
        windowHeight: 0,
        sircle:{
            r: 40,
            x: 0,
            y: 0,
            color: "yellow",
        },
        dragging: false,
        startPoint:{
            x: 0,
            y: 0,
        },
        sircleReferencePoint:{
            x: 0,
            y: 0,
        }
    },
    // computed: {

    // },
    methods: {
        zoom: function(event){
            this.sircle.r -= event.deltaY/5;
            console.log(event.clientX);
            console.log(event.clientY);
        },

        startDrag: function (e) {
            e = e.changedTouches ? e.changedTouches[0] : e
            this.dragging = true
            this.sircle.color = "blue"
            this.startPoint.x = e.clientX
            this.startPoint.y = e.clientY
            this.sircleReferencePoint.x = this.sircle.x
            this.sircleReferencePoint.y = this.sircle.y
          },
        onDrag: function (e) {
            e = e.changedTouches ? e.changedTouches[0] : e
            if (this.dragging) {
                this.sircle.x = this.sircleReferencePoint.x + (e.clientX - this.startPoint.x)
                this.sircle.y = this.sircleReferencePoint.y + (e.clientY - this.startPoint.y)
            }
        },
        stopDrag: function (e) {
            if (this.dragging) {
                this.dragging = false
                this.sircle.color = "yellow"
            }
        }


        

    },
    watch: {
        'sircle.r' : function(newR){
            console.log('R = %s ', newR)
        }

    },
    mounted(){
        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;
        this.sircle.x = this.windowWidth/2;
        this.sircle.y = this.windowHeight/2;
    },
    updated(){
    }
})

